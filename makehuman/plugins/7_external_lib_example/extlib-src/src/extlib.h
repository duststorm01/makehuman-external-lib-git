#pragma once

/**
 * Just a trivial library that does not actually do much.
 **/

class MhDefInterface
{
public:
    MhDefInterface(void);

    void doAction(float *vertex_coords, unsigned int nb_coords);

    ~MhDefInterface(void);
};
