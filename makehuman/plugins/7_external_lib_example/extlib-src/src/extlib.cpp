#include "extlib.h"
#include "math.h"

MhDefInterface::MhDefInterface(void){}

MhDefInterface::~MhDefInterface(void){}

void MhDefInterface::doAction(float *vertex_coords, unsigned int nb_coords)
{
    static float t = 0.f;

    for (unsigned int i = 0; i < nb_coords*3; i++) {
        vertex_coords[i] += (sin(t) / 10.0f);

        t += 0.1f;
    }
}

