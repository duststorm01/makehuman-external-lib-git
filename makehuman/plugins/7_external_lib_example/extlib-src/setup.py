##
# Cython build script (distutils)
##

from distutils.core import setup,Extension
#from distutils.extension import Extension
#from Cython.Distutils import build_ext
from Cython.Build import cythonize
from Cython.Distutils import build_ext

import numpy
import os

#setup(
#    ext_modules = cythonize("extlib.pyx")  # Auto cythonize using comments in pyx files
#)


ext = Extension(
        "extlib",
        sources = [
                "pyextlib.pyx",
                "src/extlib.cpp"
        ],
        include_dirs = ['.','src'],
        language="c++"
    )


setup (
    name = "python-extlib",
    version = 1,
    description = "Test for wrapping a library called extlib (external lib) for use in python",
    author = "Jonas Hauquier",
    #author_email = "test@domain.com",
    url = "http://makehuman.org",
    license = 'AGPL3',
    ext_modules=[ext, ],
    cmdclass = {
            'build_ext': build_ext,
    },
)
