# distutils: language = c++
# distutils: sources = src/extlib.cpp

# The comment block above is not just comments, it is used by cythonize.
# Keep it at the top!

##
# Python extension module for accessing the C++ library in python
##

# Import cython library
from cextlib cimport MhDefInterface

import numpy
cimport numpy

import cython


cdef MhDefInterface *_interface
_interface_defined = False

cdef MhDefInterface* getSingleton():
    """
    Create and always return the same instance of the interface object.
    """
    global _interface_defined
    if not _interface_defined:
        global _interface
        _interface = new MhDefInterface()
        _interface_defined = True
    return _interface



@cython.boundscheck(False)
@cython.wraparound(False)
def doAction(numpy.ndarray[numpy.float32_t, ndim=2, mode="c"] coords not None):
    """
    Perform action on external interface.
        coords is a numpy float32 array
        Result is returned by modifying array in place
    """
    interface = getSingleton()

    # numpy array is passed as row-major contiguous array (x0,y0,z0,x1,y1,z1, ...)
    interface.doAction(&coords[0,0], coords.shape[0])

