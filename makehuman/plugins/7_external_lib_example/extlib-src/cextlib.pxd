##
# Wrapper around the extlib C++ library with interface defined in extlib.h
#
# Note: this pxd must have a different name than the .pyx file!
##

cdef extern from "src/extlib.h":
    cdef cppclass MhDefInterface:
        MhDefInterface()
        void doAction(float *vertex_coords, unsigned int nb_coords)


