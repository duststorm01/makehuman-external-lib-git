#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

"""
MakeHuman library integration example plugin.

**Project Name:**      MakeHuman

**Product Home Page:** http://www.makehuman.org/

**Code Home Page:**    https://bitbucket.org/MakeHuman/makehuman/

**Authors:**           Jonas Hauquier

**Copyright(c):**      MakeHuman Team 2001-2014

**Licensing:**         AGPL3 (http://www.makehuman.org/doc/node/the_makehuman_application.html)

    This file is part of MakeHuman (www.makehuman.org).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

**Coding Standards:**  See http://www.makehuman.org/node/165

Abstract
--------

Example of a plugin that can call an external library and visualize the result
in MakeHuman.
This example shows how it is possible to wrap a C or C++ (opensource) library
using cython and make it read and modify Makehuman mesh data to directly
interface with the MH mesh, at the speed of native C++ applications.

Before this plugin works, the native library and its wrapper first have to be
compiled. Have a look in the extlib-src folder. In that folder you'll find a
prepared build.sh script. After building, there should be a .so or .pyd file
in the plugin folder (the folder of the file you're reading right now).
"""

import gui3d
import mh
import gui
import log

# Load extlib.so (or extlib.pyd on windows) cython module from this package
from . import extlib

class LibraryTestTaskView(gui3d.TaskView):
    """
    This class defines a tab in the MH GUI.
    """

    def __init__(self, category):
        # Here we choose the name of the tab:
        gui3d.TaskView.__init__(self, category, 'External lib example')

        # Add a basic GUI group element and a button
        box = self.addLeftWidget(gui.GroupBox('Tools'))
        
        self.runButton = box.addWidget(gui.Button('Run'))
        
        # Event to fire when the button is clicked
        @self.runButton.mhEvent
        def onClicked(event):
            self.callLibrary(self.human)

        # For backing up original human data so we can restore it
        self.meshStored = None
        self.meshStoredNormals = None

        self.human = gui3d.app.selectedHuman

    def callLibrary(self, human):
        """
        Invoke call to external library with human mesh data.
        """
        # Make backup of mesh
        self.storeMesh(self.human)

        # Call to cython wrapped C module, modify human verts
        extlib.doAction(human.meshData.coord)


        ## These could have a performance penalty if you're aiming for realtime
        ## animation. Instead of passing .coord to the external lib, you can
        ## also pass the .r_coord and .r_vnorm members and animate those directly
        ## (these are the raw OpenGL vertex buffer data)

        # Mark that coordinates have to be updated (to OpenGL)
        self.human.meshData.markCoords(coor=True)

        # Recalculate normals (will mark normals for update too)
        self.human.meshData.calcNormals()

        # If human is smoothed (we only updated the original verts now), also
        # update the subdivided mesh
        if self.human.isSubdivided():
            self.human.updateSubdivisionMesh()

        # Update vertex coordinate and normal attributes in OpenGL vertex buffer
        self.human.meshData.update()

    def storeMesh(self, human):
        """
        Store (original) mesh vertex coordinates and normals.
        """
        log.message("Storing mesh status")
        self.meshStored = human.meshData.coord.copy()
        self.meshStoredNormals = human.meshData.vnorm.copy()

    def restoreMesh(self, human):
        """
        Restore previously stored (original) mesh vertex coordinates and
        normals.
        """
        if self.meshStored is None:
            return
        log.message("Restoring mesh status")
        human.meshData.coord[...] = self.meshStored
        human.meshData.vnorm[...] = self.meshStoredNormals
        human.meshData.markCoords(coor=True, norm=True)
        human.meshData.update()

        # Update subdivided mesh if mesh is smoothed
        if human.isSubdivided():
            human.updateSubdivisionMesh()

        # Update proxy mesh too if there is one attached
        human.updateProxyMesh()

        self.meshStored = None
        self.meshStoredNormals = None

    def onShow(self, event):
        """
        Executed when this plugin is shown.
        """
        pass

    def onHide(self, event):
        """
        Executed when this plugin is hidden.
        """
        # When clicking to another plugin restore the original mesh state
        self.restoreMesh(self.human)

category = None
taskview = None


def load(app):
    """
    This method is called when the plugin is loaded into makehuman
    The app reference is passed so that a plugin can attach a new category, task, or other GUI elements
    """
    # Choose category tab to show plugin under (in MH GUI):
    category = app.getCategory('Utilities')

    # Create the plugin GUI
    taskview = category.addTask(LibraryTestTaskView(category))


def unload(app):
    """
    This method is called when the plugin is unloaded from makehuman
    At the moment this is not used, but in the future it will remove the added GUI elements
    """
    pass
